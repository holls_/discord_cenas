const Discord = require('discord.js')
const client = new Discord.Client();
const { appToken, prefix } = require('./config.json')
const linoServer = require('./chats/linoTakeMyEnergy.guild')
const servidorBot = require('./chats/servidorBot.guild')

client.on('ready', () => {
    console.log(`Looged in as ${client.user.tag}!!!`)
})

client.on('message', msg => {
    /**
     * Como aceder ao nome no canal de texto .... (no msg)
     */
    console.log(msg.channel.guild.name + '\n')
    console.log(msg.content)
    linoServer(msg, prefix)
    servidorBot(msg)
})

client.login(appToken)
module.exports = client
/**
 * Template link para adicionar um bot
 * https://discordapp.com/oauth2/authorize?client_id=123456789012345678&scope=bot
 * O do meu bot
 * https://discordapp.com/oauth2/authorize?client_id=560569209529303051&scope=bot&permissions=0
 */