const { prefix } = require('../config.json')
const moment = require('moment')
// console.log(moment)

function calculateTimeLeft() {
    let actualDate = moment().unix()
    let finalDate = moment("2019-06-14").unix()

    /**
     * VAi ser preciso uma regra de três simples 
     * depois de subtrair as datas
     */

    let timeLeftms = finalDate - actualDate
    console.table({ data_atual: actualDate, data_entrega: finalDate, tempo_que_falta: timeLeftms })
    return Math.round(timeLeftms / 86400);
}

let servidorBot = function (msg) {
    if (msg.content.startsWith(prefix)) {
        if (msg.content == prefix + 'quantofalta') msg.reply(`Faltam ${calculateTimeLeft()} dias!!!`)

    }
}

module.exports = servidorBot;