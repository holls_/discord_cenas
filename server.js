const express = require('express')
const bodyParser = require('body-parser')

const port = 3000;

const app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
    res.send('Express com Discord API')
})


app.listen(port, () => console.log('A correr...!!!!\n(Tudo no loader.js)'))

module.exports = app;